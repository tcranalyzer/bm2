FROM r-base:4.3.2

RUN apt update -y
RUN apt upgrade -y
RUN apt install build-essential libcurl4-gnutls-dev libxml2-dev libssl-dev libfontconfig1-dev libharfbuzz-dev  libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev cmake -y
RUN apt install procps curl file git -y 

RUN R -e "install.packages('qs')"
RUN R -e "install.packages('seqinr')"
RUN R -e "install.packages('data.table')"

RUN useradd -m -s /bin/zsh linuxbrew && \
    usermod -aG sudo linuxbrew &&  \
    mkdir -p /home/linuxbrew/.linuxbrew && \
    chown -R linuxbrew: /home/linuxbrew/.linuxbrew
USER linuxbrew
RUN /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
USER root
RUN chown -R $CONTAINER_USER: /home/linuxbrew/.linuxbrew
ENV PATH="/home/linuxbrew/.linuxbrew/bin:${PATH}"
RUN git config --global --add safe.directory /home/linuxbrew/.linuxbrew/Homebrew
USER linuxbrew

RUN brew install mmseqs2

USER root

WORKDIR /app
COPY . /app/

ENTRYPOINT ["sh", "Devel/base2.sh"]
if [ "$study_name" = "" ]; then
    echo "No study_name set. Exiting."
else
    Rscript Devel/BasisModule2a.R $study_name

    mkdir /app/TCR_Cluster
    mkdir /app/TCR_Cluster_Results

    cp -a /app/Data/Data/02_Fasta/. /app/TCR_Cluster/

    for filename in /app/TCR_Cluster/*.fasta; do
        name=$(basename "$filename" ".fasta")
        echo "Processing $name"
        mmseqs easy-cluster $filename /app/TCR_Cluster_Results/cluster_results_$name tmp
    done
    
    cp -a /app/TCR_Cluster_Results/. /app/Data/Data/02_Fasta/

    Rscript Devel/BasisModule2b.R $study_name
fi
